import { MultiChatSocket, MultiChatWindow, useMultiChatLogic } from 'react-chat-engine-advanced'

const ChatsPage = (props) => {
  const chatProps = useMultiChatLogic('37411313-b49c-4d47-8f0f-8bde676559b8', props.user.usernmae, props.user.secret)

  return (
    <div style={{ height: '100vh' }} >
      <MultiChatSocket {...chatProps} />
      <MultiChatWindow {...chatProps} style={{ height: '100%' }} />

    </div >
  )
}

export default ChatsPage;